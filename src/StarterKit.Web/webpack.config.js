﻿/* eslint-disable */
var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var pkg = require('./package.json');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// bundle npm packages in a separate vendor bundle
var vendorPackages = Object.keys(pkg.dependencies).filter(function (el) {
  return el.indexOf('font') === -1; // exclude font packages from vendor bundle
});

var config = {
  debug: true,
  devtool: 'eval-source-map',
  cache: true,
  entry: {
    main: path.join(__dirname, "app", "index.js"),
    vendor: vendorPackages
  },
  output: {
    path: path.join(__dirname, "wwwroot", "js"),
    filename: process.env.NODE_ENV === 'production' ? '[name].min.js' : '[name].js',
    sourceMapFilename: '[file].map',
  },
  resolve: {
    modulesDirectories: ['node_modules']
  },
  plugins: [
    new webpack.OldWatchingPlugin(), //needed to make watch work. see http://stackoverflow.com/a/29292578/1434764
    new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.js"),
    new ExtractTextPlugin('../css/[name].css')
  ],
  resolveLoader: {
    'fallback': path.join(__dirname, 'node_modules')
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'app'),
        exclude: /node_modules/,
        loader: 'babel',
        query: { presets: ['es2015', 'react'] }
      },
      { test: /(\.css)$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader?sourceMap") },
      { test: /\.(jpg|png)$/, include: path.join(__dirname, 'app'), loader: 'file?name=../images/[name].[ext]' },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&name=../fonts/[name].[ext]',
      }
    ]
  }
}

/*
 * Optimize output for production
 */
if (process.env.NODE_ENV === 'production') {
  config.debug = false;
  config.devtool = false;
  config.plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      comments: false,
      compress: { warnings: false }
    }),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.min\.css$/,
      cssProcessorOptions: { discardComments: { removeAll: true } }
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify('production') }
    })
  ];
}

module.exports = config;
