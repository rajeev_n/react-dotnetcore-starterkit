import React from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import configureStore from './store/reduxStore';
import reducers from './reducers/index';
import routes from './routes';
import ExpenseActions from './actions/expenseActions';
import './styles/site.css';
import Bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import FontAwesome from 'font-awesome/css/font-awesome.min.css';

/* Load Items before app starts */
const store = configureStore();
// store.dispatch(ExpenseActions.fetchExpenses());

render((
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>
), document.getElementById('app'));
