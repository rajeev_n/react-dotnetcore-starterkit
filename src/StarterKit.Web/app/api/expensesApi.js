import 'whatwg-fetch';
import 'babel-polyfill';
import constants from '../constants';

const API_HEADERS = {
  'Content-Type': 'application/json',
};

let ExpensesApi = {

  fetchExpenses() {
    return fetch(`${constants.API_URL}/expenses`, { headers: API_HEADERS })
      .then((response) => response.json());
  },

  addExpense(expense) {
    return fetch(`${constants.API_URL}/expenses`, {
      method: 'post',
      headers: API_HEADERS,
      body: JSON.stringify(expense)
    })
      .then((response) => response.json());
  },

  updateExpense(expense) {
    return fetch(`${constants.API_URL}/expenses/${expense.ExpenseId}`, {
      method: 'put',
      headers: API_HEADERS,
      body: JSON.stringify(expense)
    });
  },

  deleteExpense(expenseId) {
    return fetch(`${constants.API_URL}/expenses/${expenseId}`, {
      method: 'delete',
      headers: API_HEADERS
    });
  }

};

export default ExpensesApi;
