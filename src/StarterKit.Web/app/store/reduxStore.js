import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import throttle from "redux-throttle";
import reducers from '../reducers/index';

const defaultThrottleOption = { // https://lodash.com/docs#throttle
  leading: true,
  trailing: false
};

const throttleMiddleWare = throttle(500, defaultThrottleOption);    //default 500ms, 

function configureStore(initialState) {
  return createStore(
    reducers,
    initialState,
    applyMiddleware(thunk, throttleMiddleWare)
  );
}

export default configureStore;
