import { dispatchAsync } from '../ReduxDispatcher';
import constants from '../constants';
import ExpensesApi from '../api/expensesApi';

let ExpenseActions = {
  fetchExpenses() {
    return (dispatch) => {
      dispatchAsync(ExpensesApi.fetchExpenses(), dispatch, {
        request: constants.FETCH_EXPENSES
      });
    };
  },

  addExpense(expense) {
    return (dispatch) => {
      dispatchAsync(ExpensesApi.addExpense(expense), dispatch, {
        request: constants.ADD_EXPENSE,
      }, { expense });
    };
  },

  deleteExpense(expenseId) {
    return (dispatch) => {
      dispatchAsync(ExpensesApi.deleteExpense(expenseId), dispatch, {
        request: constants.DELETE_EXPENSE,
      }, { expenseId });
    };
  }
};

export default ExpenseActions;
