import update from 'react-addons-update';
import constants from '../constants';
import initialState from './initialState';

const expenseReducer = (state = initialState.expenses, action) => {
  switch (action.type) {

    case constants.FETCH_EXPENSES_SUCCESS: {
      return state;
    }

    case constants.SAVE_EXPENSE_SUCCESS: {
      return state;
    }

    case constants.ADD_EXPENSE_SUCCESS: {
      return state;
    }

    default:
      return state;
  }
};

export default expenseReducer;
