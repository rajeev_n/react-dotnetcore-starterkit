import { combineReducers } from 'redux';
import expense from './expenseReducer';

const reducers = combineReducers({
  expense
});

export default reducers;
