import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import Home from './components/home/HomePage';
import Expenses from './components/expenses/ExpensesPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="expenses" component={Expenses} />
  </Route>
);
