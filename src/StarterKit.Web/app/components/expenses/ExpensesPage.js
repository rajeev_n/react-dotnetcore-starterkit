import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ExpenseActions from '../../actions/expenseActions';
import {
  Button,
  Table,
  Label,
} from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';

class ExpensesPage extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <div>Expenses Overview</div>
          <div className="pull-right">
            <Button><FontAwesome name="floppy-o" /></Button>
            <Button><FontAwesome name="undo" /></Button>
          </div>
        </div>

        <Table striped>
          <thead>
            <tr>
              <th>Title</th>
              <th>Supplier</th>
              <th>Amount</th>
              <th>Payment Method</th>
              <th>Payment Frequency</th>
              <th>Bill Notification</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Electricity</td>
              <td>RedEnergy</td>
              <td>$200.00</td>
              <td>Direct Debit</td>
              <td>Quarterly</td>
              <td>via Letter</td>
              <td>
                <Button><FontAwesome name="pencil" /></Button>
                <Button><FontAwesome name="trash-o" /></Button>
              </td>
            </tr>
            <tr>
              <td>Gas</td>
              <td>RedEnergy</td>
              <td>$110.00</td>
              <td>Direct Debit</td>
              <td>Quarterly</td>
              <td>via Email</td>
                <Button><FontAwesome name="pencil" /></Button>
                <Button><FontAwesome name="trash-o" /></Button>
            </tr>
            <tr>
              <td>Internet</td>
              <td>TPG</td>
              <td>$60.00</td>
              <td>Direct Debit</td>
              <td>Monthly</td>
              <td>via Online Account</td>
                <Button><FontAwesome name="pencil" /></Button>
                <Button><FontAwesome name="trash-o" /></Button>
            </tr>
            <tr>
              <td>Mobile</td>
              <td>AmaySim</td>
              <td>$40.00</td>
              <td>Direct Debit</td>
              <td>Monthly</td>
              <td>via Email</td>
                <Button><FontAwesome name="pencil" /></Button>
                <Button><FontAwesome name="trash-o" /></Button>
            </tr>
            <tr>
              <td>P.O. Box Rental</td>
              <td>Australia Post</td>
              <td>$121.00</td>
              <td>Direct Debit</td>
              <td>Quarterly</td>
              <td>via Email</td>
                <Button><FontAwesome name="pencil" /></Button>
                <Button><FontAwesome name="trash-o" /></Button>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

ExpensesPage.propTypes = {
  actions: PropTypes.object.isRequired
};

export default connect(
  (store) => {
    return {
    };
  },
  (dispatch) => {
    return {
      actions: bindActionCreators(ExpenseActions, dispatch)
    };
  }
)(ExpensesPage);
