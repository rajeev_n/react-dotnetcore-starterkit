﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace BudgetMe.Web.api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot($"{Directory.GetCurrentDirectory()}\\api")
                .UseWebRoot($"{Directory.GetCurrentDirectory()}\\wwwroot")
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
